package mailer

import (
	"gitee.com/zhucheer/orange/cfg"
	"errors"
)

func NewMailer(optionName string) (mailer *sendMail, err error){
	options:=cfg.Config.Exists("mailer."+optionName)

	if options == false{
		return nil, errors.New("not found mailer config by "+ optionName+"")
	}
	host:=cfg.Config.GetString("mailer."+optionName+".host")
	port:=cfg.Config.GetInt("mailer."+optionName+".port")
	username:=cfg.Config.GetString("mailer."+optionName+".username")
	password:=cfg.Config.GetString("mailer."+optionName+".password")
	openTsl:=cfg.Config.GetBool("mailer."+optionName+".tsl")

	mailerOption:=Mailer{
		Host:host,
		Port:port,
		UserName:username,
		Password:password,
		OpenTsl:openTsl,
	}
	mailer,err= GetSendMailer(mailerOption)

	return
}
