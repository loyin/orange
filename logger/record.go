package logger

import (
	"fmt"
	"os"
	"path/filepath"
	"runtime"
)

// Record
type Record struct {
	Level       LogLevel `json:"-"`
	LevelString string   `json:"level"`
	Message     string   `json:"message"`
	Pid         int      `json:"-"`
	Program     string   `json:"program"`
	Time        string   `json:"time"`
	FuncName    string   `json:"func_name"`
	Path        string   `json:"path"`
	FileName    string   `json:"file_name"`
	Line        int      `json:"line"`
	Color       string   `json:"-"`
	ColorClear  string   `json:"-"`
}

// GetMessageRecord, make a record and return it's reference.
func GetMessageRecord(level LogLevel, format string, a ...interface{}) *Record {
	message := fmt.Sprintf(format, a...)
	pc, file, line, _ := runtime.Caller(4)
	record := &Record{
		Level:       level,
		Message:     message,
		Pid:         os.Getpid(),
		Program:     filepath.Base(os.Args[0]),
		Time:        "",
		FuncName:    runtime.FuncForPC(pc).Name(),
		Path:        file,
		FileName:    filepath.Base(file),
		Line:        line,
		Color:       LevelColorFlag[level],
		ColorClear:  LevelColorSetClear,
		LevelString: LevelString[level],
	}
	return record
}
