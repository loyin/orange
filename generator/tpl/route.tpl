package http

import (
	"{{.projectName}}/http/controller"
	"{{.projectName}}/http/middleware"
	"gitee.com/zhucheer/orange/app"
)

type Route struct {
}

func (s *Route) ServeMux() {
	commonGp := app.NewRouter("")
	{
		commonGp.GET("/", controller.Welcome)

		commonGp.GET("/api", func(ctx *app.Context) error {
			return ctx.ToJson(map[string]interface{}{"data": "orange framework"})
		})
	}

	authGp := app.NewRouter("/auth", middleware.NewAuth())
	{
		authGp.ALL("/info", controller.AuthCheck)
	}

}