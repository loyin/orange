package main

import (
	"gitee.com/zhucheer/orange/app"
	"{{.projectName}}/http"
)

func main() {

	router := &http.Route{}
	app.AppStart(router)

}