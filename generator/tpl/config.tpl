[app]
    name = "orange"
    key = "{{.appkey}}"
    httpAddr = "127.0.0.1"
    httpPort = 8088
    maxBody = 2096157
    csrfVerify = true
    maxWaitSecond = 120
    pidfile = "./storage/orange.pid"
    viewPath = "./storage/views"
    [app.logger]
        level = "INFO"
        type = "text"
        path = ""
        syncInterval = 200
    [app.session]
        isOpen = true
        driver = "cookie"
        timeout = 1800
    [app.upload]
        storage = "./storage/allimg"
        maxSize = 2096157
        ext = ["jpg","jpeg","gif","png"]