package controller

import (
	"gitee.com/zhucheer/orange/app"
)

func Welcome(c *app.Context) error {

	return c.ToString(`{{.indexHtml}}`)
}

func AuthCheck(c *app.Context) error {

	return c.ToJson(map[string]interface{}{
		"auth": "auth is ok",
	})
}