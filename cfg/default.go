package cfg

//默认配置,配置文件不存在时会加载默认配置

var defaultConfig = `[app]
    name = "orange"
    key = "orangeDefaultConfig"
    httpAddr = "0.0.0.0"
    httpPort = 8088
    maxBody = 2096157
    csrfVerify = true
    pprofOpen = true
    maxWaitSecond = 60
	pidfile = "./storage/orange.pid"
    viewPath = "./storage/views"
    [app.logger]
        level = "INFO"
        type = "text"
        path = ""
        syncInterval = 2
    [app.session]
        isOpen = false
        timeout = 1800
    [app.upload]
        storage = "./storage/allimg"
        maxSize = 2096157
        ext = ["jpg"]
[mailer]

[database]
    initCap = 2
    maxCap = 5
    idleTimeout = 5
    debug = true
    [database.mysql]

    [database.redis]
`
