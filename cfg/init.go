package cfg

import (
	"fmt"
	"gitee.com/zhucheer/cfg"
	"gitee.com/zhucheer/orange/utils"
)

var Config = cfg.New("")
var ConfigDef = cfg.New("")

func init() {
	NewFlagParam()
	// 注册配置文件参数
	SetStringFlag("config", "./config/config.toml", "config file path")
}

func StartConfig() {
	configPath := GetStringFlag("config")
	if isFile, _ := utils.FileExists(configPath); isFile == false {
		fmt.Println(fmt.Sprintf("\033[0;33m%v \033[0m", "[WARNING] config.toml not found, please check the config path"))
		Config = cfg.NewData(defaultConfig)
	} else {
		Config = cfg.New(configPath)
	}

	if Config == nil {
		panic("config init failed")
	}

	// 加载默认配置对象
	ConfigDef = cfg.NewData(defaultConfig)
}

// 门面方法快速调用

// GetString 获取字符型配置
func GetString(key string, defaultVal string) string {
	if Config.Exists(key) {
		return Config.GetString(key)
	}
	return defaultVal
}

// GetInt 获取int类型配置
func GetInt(key string, defaultVal int) int {
	if Config.Exists(key) {
		return Config.GetInt(key)
	}
	return defaultVal
}

// GetBool 获取bool类型配置
func GetBool(key string, defaultVal bool) bool {
	if Config.Exists(key) {
		return Config.GetBool(key)
	}
	return defaultVal
}
