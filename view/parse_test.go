package view

import (
	"testing"
)

func TestTextPath(t *testing.T) {

	res := TextPath("./view_test.tpl", map[string]interface{}{
		"word": "orange api framework",
	})
	if res != "Hello orange api framework" {
		t.Error("TextPath have an error#1")
	}
}

func TestHtmlPath(t *testing.T) {
	res := HtmlPath("./html_test.tpl", map[string]interface{}{
		"Title":    "faster and smarty",
		"Content":  "<div>Test html content</div>",
		"Content2": "<a href='#'>Test html link</a>",
	})

	willRes := `<html><head><title>Orange Framework faster and smarty</title></head><body>&lt;div&gt;Test html content&lt;/div&gt;<a href='#'>Test html link</a><body><html>`

	if res != willRes {
		t.Error("HtmlPath have an error#1")
	}
}
