package view

import (
	"gitee.com/zhucheer/orange/cfg"
	"strings"
	"testing"
)

func startCfg() {
	cfg.ParseParams()
	cfg.StartConfig()
}

// go test view_test.go view.go parse.go -v -args --config=../project/config/config.toml
func TestParseViewName(t *testing.T) {
	startCfg()
	path1 := parseViewName("index")
	if strings.Contains(path1, "./storage/views/index.tpl") == false {
		t.Error("parseViewName have an error #1")
	}

	path2 := parseViewName("home.index")
	if strings.Contains(path2, "./storage/views/home/index.tpl") == false {
		t.Error("parseViewName have an error #2")
	}
}

func TestViewAssigns(t *testing.T) {
	startCfg()

	showData := map[string]interface{}{
		"Title": "Orange view",
	}
	showData2 := struct {
		Title2 string
	}{
		Title2: "Orange view2",
	}

	tmpl := ContextTmpl("home.index").Assigns(showData).Assigns(showData2).Assign("Content", "hello")
	if len(tmpl.ShowData) != 3 {
		t.Error("Assigns have an error #1")
	}

	if strings.Contains(tmpl.ViewPath, "./storage/views/home/index.tpl") == false {
		t.Error("Assigns have an error #2")
	}

	tmpl.Render()
}
