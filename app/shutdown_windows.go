package app

import (
	"context"
	"fmt"
	"os"
	"os/signal"
	"syscall"
)

// listenShutDownSign 监听退出信号
func listenShutDownSign(ctx context.Context, orangeSrv *OrangeServer) {
	exitSig := []os.Signal{
		syscall.SIGHUP, syscall.SIGINT, syscall.SIGTERM, syscall.SIGQUIT, syscall.SIGTERM,
	}

	appsign := make(chan os.Signal, 1)
	signal.Notify(appsign, exitSig...)

	select {
	case signType := <-appsign:
		fmt.Println(fmt.Sprintf("[ORANGE] \033[0;33m shutdown sign:%v \033[0m ", signType.String()))
		shutdownDo(ctx, orangeSrv)
	}
	signal.Stop(appsign)

	// http服务关闭后等待业务处理完成后关闭应用
	exitWaitHandler.wg.Wait()
}
