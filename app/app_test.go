package app


import (
	"testing"
	"sync"
	"fmt"
	"time"
)

func TestAppDefer(t *testing.T) {

	AppDefer(func() {})
	AppDefer(func() {}, func() {})

	if len(exitWaitHandler.deferFuns) != 3{
		t.Error("AppDefer func have an error #1")
	}

	wg := sync.WaitGroup{}

	wg.Add(2)
	go func() {
		defer wg.Done()
		AppDefer(func() {})
	}()
	go func() {
		defer wg.Done()
		AppDefer(func() {}, func() {})
	}()
	wg.Wait()

	if len(exitWaitHandler.deferFuns) != 6{
		t.Error("AppDefer func have an error #2")
	}

}

func TestListenStop(t *testing.T) {
	appSig1:= make(chan StopSignal)
	ListenStop(appSig1)

	appSig2:= make(chan StopSignal, 1)
	ListenStop(appSig2)

	appSig3:= make(chan StopSignal, 2)
	ListenStop(appSig3)

	if len(exitWaitHandler.stopSignList) != 3{
		t.Error("TestListenStop have an error #1")
	}
	sendAppStop()

	go func() {
		select{
		case <-appSig1:
			fmt.Println("app stop appSig1")

		}
	}()


	go func() {
		select{
		case <-appSig2:
			fmt.Println("app stop appSig2")

		}
	}()

	go func() {
		select{
		case <-appSig3:
			fmt.Println("app stop appSig3")

		}
	}()

	time.Sleep(time.Second)
}