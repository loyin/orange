package app

import (
	"net/http"
	"net/http/pprof"
)

type pprofRoute struct {
	Path        string
	HandlerFunc func(w http.ResponseWriter, r *http.Request)
}

var debupApi = []pprofRoute{
	{"/debug/pprof/", pprof.Index},
	{"/debug/pprof/cmdline", pprof.Cmdline},
	{"/debug/pprof/profile", pprof.Profile},
	{"/debug/pprof/symbol", pprof.Symbol},
	{"/debug/pprof/trace", pprof.Trace},
}

func runPprof(httpSrv *http.ServeMux) {
	for _, item := range debupApi {
		httpSrv.HandleFunc(item.Path, item.HandlerFunc)
		consoleRouter("DEBUG", item.Path)
	}
}
